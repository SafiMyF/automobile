﻿#pragma checksum "..\..\Compare.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "B2D6DA00380D53710289AB8BF2AB4576"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Automobile {
    
    
    /// <summary>
    /// Compare
    /// </summary>
    public partial class Compare : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 7 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Automobile.Compare compare;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid mainLayout;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button closeCompare;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgCompareCar1;
        
        #line default
        #line hidden
        
        
        #line 131 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblCompareCar1;
        
        #line default
        #line hidden
        
        
        #line 176 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgCompareCar2;
        
        #line default
        #line hidden
        
        
        #line 177 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblCompareCar2;
        
        #line default
        #line hidden
        
        
        #line 225 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgCompareCar1s;
        
        #line default
        #line hidden
        
        
        #line 226 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblCompareCar1s;
        
        #line default
        #line hidden
        
        
        #line 234 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgCompareCar2s;
        
        #line default
        #line hidden
        
        
        #line 235 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblCompareCar2s;
        
        #line default
        #line hidden
        
        
        #line 369 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEmailId;
        
        #line default
        #line hidden
        
        
        #line 371 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSend;
        
        #line default
        #line hidden
        
        
        #line 404 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer petrolScrollView;
        
        #line default
        #line hidden
        
        
        #line 412 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPetrolVariant;
        
        #line default
        #line hidden
        
        
        #line 425 "..\..\Compare.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblPetrolCarName;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Renault;component/compare.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Compare.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.compare = ((Automobile.Compare)(target));
            return;
            case 2:
            this.mainLayout = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.closeCompare = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\Compare.xaml"
            this.closeCompare.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.closeCompare_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 4:
            this.imgCompareCar1 = ((System.Windows.Controls.Image)(target));
            return;
            case 5:
            this.lblCompareCar1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.imgCompareCar2 = ((System.Windows.Controls.Image)(target));
            return;
            case 7:
            this.lblCompareCar2 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.imgCompareCar1s = ((System.Windows.Controls.Image)(target));
            return;
            case 9:
            this.lblCompareCar1s = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.imgCompareCar2s = ((System.Windows.Controls.Image)(target));
            return;
            case 11:
            this.lblCompareCar2s = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.txtEmailId = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.btnSend = ((System.Windows.Controls.Button)(target));
            
            #line 371 "..\..\Compare.xaml"
            this.btnSend.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnSend_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 14:
            this.petrolScrollView = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 407 "..\..\Compare.xaml"
            this.petrolScrollView.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.petrolScrollView_ManipulationBoundaryFeedback_1);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btnPetrolVariant = ((System.Windows.Controls.Button)(target));
            return;
            case 16:
            this.lblPetrolCarName = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

