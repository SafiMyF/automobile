﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automobile
{
    /// <summary>
    /// Interaction logic for Compare.xaml
    /// </summary>
    public partial class Compare : UserControl
    {
        public Compare()
        {
            InitializeComponent();
        }
        public event EventHandler eventCloseCompare;
        private void closeCompare_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            eventCloseCompare(this, null);
        }

        private void btnSend_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {

        }

        private void petrolScrollView_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
