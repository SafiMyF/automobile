﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automobile
{
    /// <summary>
    /// Interaction logic for Enquiry.xaml
    /// </summary>
    public partial class Enquiry : UserControl
    {
        public Enquiry()
        {
            InitializeComponent();
        }
        public event EventHandler eventCloseEnquiry;
        private void closeEnquiry_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            eventCloseEnquiry(this, null);
        }

        private void txtQuery_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
