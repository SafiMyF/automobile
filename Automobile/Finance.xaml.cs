﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automobile
{
    /// <summary>
    /// Interaction logic for Finance.xaml
    /// </summary>
    public partial class Finance : UserControl
    {
        public Finance()
        {
            InitializeComponent();
        }
        public event EventHandler eventCloseFinance;
        private void closeFinance_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            eventCloseFinance(this, null);
        }

        private void chkInteresedInFinance_Checked(object sender, RoutedEventArgs e)
        {
            brdEmail.Visibility = Visibility.Visible;
            txtEmail.Visibility = Visibility.Visible;
        }

        private void chkInteresedInFinance_Unchecked(object sender, RoutedEventArgs e)
        {
            brdEmail.Visibility = Visibility.Collapsed;
            txtEmail.Visibility = Visibility.Collapsed;

        }

    }
}
