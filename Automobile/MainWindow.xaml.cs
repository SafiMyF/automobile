﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automobile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void svCarList_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        private void btnCar_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            homePanel.Visibility = Visibility.Hidden;
            DetailInformation di = new DetailInformation();
            parentPanel.Children.Add(di);
            btnCompare.Visibility = Visibility.Visible;
            headerBg.Visibility = Visibility.Visible;
            Storyboard Home_LargeImg_SB = this.TryFindResource("Home_LargeImg_SB") as Storyboard;
            Home_LargeImg_SB.Stop();
            //Raise DetailInformation Events here
            di.eventCloseDI += di_eventCloseDI;
            di.eventOpenScheduelTestDrive += di_eventOpenScheduelTestDrive;
            di.eventOpenFinance += di_eventOpenFinance;
            di.eventOpenEnquiry += di_eventOpenEnquiry;
        }

        void di_eventOpenScheduelTestDrive(object sender, EventArgs e)
        {
            ScheduleTestDrive std = new ScheduleTestDrive();
            parentPanel.Children.Add(std);
            //Raise Events from ScheduleTestDrive
            std.eventCloseScheduleTestDrive += std_eventCloseScheduleTestDrive;
        }

        void std_eventCloseScheduleTestDrive(object sender, EventArgs e)
        {
            ScheduleTestDrive std = (ScheduleTestDrive)sender;
            parentPanel.Children.Remove(std);
        }

        void di_eventOpenFinance(object sender, EventArgs e)
        {
            Finance fin = new Finance();
            parentPanel.Children.Add(fin);
            //Raise events from Finance
            fin.eventCloseFinance += fin_eventCloseFinance;
        }

        void fin_eventCloseFinance(object sender, EventArgs e)
        {
            Finance fin = (Finance)sender;
            parentPanel.Children.Remove(fin);
        }

        void di_eventOpenEnquiry(object sender, EventArgs e)
        {
            Enquiry enq = new Enquiry();
            parentPanel.Children.Add(enq);
            //Raise events from Enquiry
            enq.eventCloseEnquiry+=enq_eventCloseEnquiry;
        }

        void enq_eventCloseEnquiry(object sender, EventArgs e)
        {
            Enquiry enq = (Enquiry)sender;
            parentPanel.Children.Remove(enq);
        }

        void di_eventCloseDI(object sender, EventArgs e)
        {
            DetailInformation di = (DetailInformation)sender;
            parentPanel.Children.Remove(di);
            homePanel.Visibility = Visibility.Visible;
            btnCompare.Visibility = Visibility.Collapsed;
            headerBg.Visibility = Visibility.Hidden;
            Storyboard Home_LargeImg_SB = this.TryFindResource("Home_LargeImg_SB") as Storyboard;
            Home_LargeImg_SB.Begin();
            Storyboard Home_Button_SB = this.TryFindResource("Home_Button_SB") as Storyboard;
            Home_Button_SB.Begin();
        }

        private void btnCompare_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (checkCompare())
            {
                Compare cmp = new Compare();
                parentPanel.Children.Add(cmp);
                //Raise events from Compare here
                cmp.eventCloseCompare += cmp_eventCloseCompare;
            }
        }

        private bool checkCompare()
        {
            foreach (UserControl item in parentPanel.Children)
            {
                if (item is Compare)
                {
                    return false;
                }
            }
            return true;
        }

        void cmp_eventCloseCompare(object sender, EventArgs e)
        {
            Compare cmp = (Compare)sender;
            parentPanel.Children.Remove(cmp);
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            Storyboard Home_LargeImg_SB = this.TryFindResource("Home_LargeImg_SB") as Storyboard;
            Home_LargeImg_SB.Begin();
            Storyboard Home_Button_SB = this.TryFindResource("Home_Button_SB") as Storyboard;
            Home_Button_SB.Begin();
        }

        private void btnHome_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            parentPanel.Children.Clear();
            homePanel.Visibility = Visibility.Visible;
            btnCompare.Visibility = Visibility.Collapsed;
            headerBg.Visibility = Visibility.Hidden;
            Storyboard Home_LargeImg_SB = this.TryFindResource("Home_LargeImg_SB") as Storyboard;
            Home_LargeImg_SB.Begin();

            Storyboard Home_Button_SB = this.TryFindResource("Home_Button_SB") as Storyboard;
            Home_Button_SB.Begin();

        }
    }
}
