﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automobile
{
    /// <summary>
    /// Interaction logic for DetailInformation.xaml
    /// </summary>
    public partial class DetailInformation : UserControl
    {
        public event EventHandler eventCloseDI;
        public event EventHandler eventOpenScheduelTestDrive;
        public event EventHandler eventOpenFinance;
        public event EventHandler eventOpenEnquiry;
        
        int currentImgCountValue = 0;
        public DetailInformation()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            currentImgCountValue = 1;
            Rotate(currentImgCountValue);
        }
        private void Rotate(int currentImgCountValue)
        {
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            string filename = ((currentImgCountValue < 25) ? "Fluence360/fluence" + currentImgCountValue + ".png" : "Fluence360/fluence" + currentImgCountValue + ".png");
            image.UriSource = new Uri(filename, UriKind.Relative);
            image.EndInit();
            carImage.Source = image;
            carImage.Stretch = Stretch.Uniform;
        }

        private void btnPrevImg_TouchDown_1(object sender, TouchEventArgs e)
        {
            currentImgCountValue--;
            if (currentImgCountValue < 1)
            {
                currentImgCountValue = 24;
            }
            Rotate(currentImgCountValue);

        }

        private void btnNextImg_TouchDown_1(object sender, TouchEventArgs e)
        {
            currentImgCountValue++;
            if (currentImgCountValue > 24)
            {
                currentImgCountValue = 1;
            }
            Rotate(currentImgCountValue);
        }

        private void closeDI_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            eventCloseDI(this, null);
        }

        private void petrolScrollView_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        private void btnScheduleTestDrive_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            eventOpenScheduelTestDrive(this, null);
        }

        private void btnFinance_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            eventOpenFinance(this, null);
        }

        private void btnEnquiry_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            eventOpenEnquiry(this, null);
        }
    }
}
